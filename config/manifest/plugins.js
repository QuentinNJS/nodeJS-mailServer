'use strict';

const async     = require('async');
const envConfig = require('../environments/all');

module.exports.init = server => {
    return new Promise((resolve, reject) => {
        async.series({
            good(done) {
                server.register({
                    register : require('good')
                }, done);
            },
            hapiSwagger(done){
                server.register({
                    register : require('hapi-swagger'),
                    options : {
                        info: {
                            'title': 'Mail API Documentation'
                        }
                    }

                },done);
            },
            blipp(done) {
                server.register({
                    register : require('blipp'),
                    options  : {
                        showStart : envConfig.log.showRouteAtStart,
                        showAuth  : true
                    }
                }, done);
            },
            socketIo(done){
                server.register({
                    register: require('hapi-io'),
                }, done);
            },
            boom(done) {
                server.register({
                    register : require('hapi-boom-decorators')
                }, done);
            },
            inert(done) {
                server.register({
                    register : require('inert')
                    }, done);
            },
            vision(done) {
                server.register({
                    register : require('vision')
                }, done);
            }
        }, err => {
            if (err) {
                reject(err);
                return;
            }

            resolve();
        });
    });
};