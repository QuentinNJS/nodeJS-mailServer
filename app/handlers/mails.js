'use strict';
const nodemailer = require('nodemailer');
const Mailgen = require('mailgen');
const Boom = require('boom');


const genBodySave= (user, uncryptedPassword) => {
    return {
        body: {
            name: user.firstname,
            intro: 'We confirm the creation of your account <br\>' +
            'Votre login : '+ user.login+'<br\>'
            +'Votre mot de passe :'+ uncryptedPassword+'<br\>',
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
        }
    };


};

const genBodyUpdate= (user) => {
    return {
        body: {
            name: user.firstname,
            intro: 'Nous vous informons que votre login et/ou votre mot de passe a été modifié',
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
        }
    };


};

const genBodyRecover= (user) => {
    return {
        body: {
            name: user.firstname,
            intro: 'Votre mot de passe a bien été modifié',
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
        }
    };


};


const mailGenerator = new Mailgen({
    theme: 'salted',
    product: {
        name: 'Mailgen',
        link: 'https://mailgen.js/'

    }
});


const createSmtpTransport = ()=> {
    return  nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: "ladressedunodeetplus@gmail.com",
            pass: "lenodejadoreca"
        }
    });

};


module.exports.sendNewUserMail = (request, reply) => {
    let transporter = createSmtpTransport();
    let subject = 'Confirmation de la création';

    let mailOptions = {
        from: '"Hapi Project" <quentin.tuyeras@etu.unilim.fr>',
        to: request.payload.user.email,
        subject: subject,
        text: mailGenerator.generatePlaintext(genBodySave(request.payload.user,request.payload.uncryptedPassword)),
        html: mailGenerator.generate(genBodySave(request.payload.user,request.payload.uncryptedPassword))
    };
    transporter.sendMail(mailOptions, (error)=> {
        if(error){
            reply(Boom.badImplementation("Email not send"));
            transporter.close();
            return;
        }
        reply(null,{'reponse' : 'email envoyé'});

        transporter.close();
    });
};

module.exports.sendUpdateUserMail = (request, reply) => {
    let transporter = createSmtpTransport();

    let subject = 'Modification du login et/ou du mot de passe';

    let mailOptions = {
        from: '"Hapi Project" <quentin.tuyeras@etu.unilim.fr>',
        to: request.payload.user.email,
        subject: subject,
        text: mailGenerator.generatePlaintext(genBodyUpdate(request.payload.user)),
        html: mailGenerator.generate(genBodyUpdate(request.payload.user))
    };

    transporter.sendMail(mailOptions, (error)=> {
        if(error){
            reply(Boom.badImplementation("Email not send"));
            transporter.close();
            return;
        }
        reply(null,{'reponse' : 'email envoyé'});

        transporter.close();
    });
};



module.exports.sendRecoverUserMail = (request, reply) => {

    let transporter = createSmtpTransport();

    let subject = 'Recuperation du mot de passe';

    let mailOptions = {
        from: '"Hapi Project" <quentin.tuyeras@etu.unilim.fr>',
        to: request.payload.user.email,
        subject: subject,
        text: mailGenerator.generatePlaintext(genBodyRecover(request.payload.user)),
        html: mailGenerator.generate(genBodyRecover(request.payload.user))
    };

    transporter.sendMail(mailOptions,(error)=> {
        if(error){
            reply(Boom.badImplementation("Email not send"));
            transporter.close();
            return;
        }

        reply(null,{'reponse' : 'email envoyé'});

        transporter.close();
    });

};


