const Joi = require('joi');
const handlerMail = require('../handlers/mails');

exports.register = (server, options, next) => {
    server.route([
        {
            method  : 'POST',
            path    : '/mails/new',
            config  : {
                description : 'Mail pour un nouvel utilisateur',
                notes       : 'Route envoyant un mail pour la creation d\'un utilisateur',
                tags        : ['api'],
                plugins     : {
                    'hapi-io' : 'new-user',
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                },
                handler     : handlerMail.sendNewUserMail

            }
        },
        {
            method  : 'POST',
            path    : '/mails/update',
            config  : {
                description : 'Mail pour un update d\'utilisateur',
                notes       : 'Route envoyant un mail pour l\'update d\'un utilisateur',
                tags        : ['api'],
                plugins     : {
                    'hapi-io' : 'update-user',
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                },
                handler     : handlerMail.sendUpdateUserMail

            }
        },
        {
            method: 'POST',
            path: '/mails/recover',
            config: {
                description: 'Mail pour une recuperation et modification de mot de passe utilisateur',
                notes: 'Route envoyant un mail pour recuperer et modifier le mot de passe d\'un utilisateur',
                tags: ['api'],
                plugins: {
                    'hapi-io': 'recover-user',
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                },
                handler: handlerMail.sendRecoverUserMail

            }
        }
    ]);
    next();
};

exports.register.attributes= {
    name: 'mail-routes'
};
